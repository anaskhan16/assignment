import React, { Component } from "react";
import Grid from "./grid";
import OrderOfActions from "./action";

class App extends Component {
  state = {
    current_floor: 0,
    actions: [],
    order: [],
  };

  addAction = (action) => {

    let actions = this.state.actions;
    actions.push(action);
    this.setState({
      actions,
    });
  };

  changeFloor = (order) => {
    console.log("Order of execution : ",order);
    order.forEach((item, i) => {
      
      setTimeout(() => {
        this.setState({
          current_floor: item.floor,
          actions : this.state.order.filter(action => (action.floor === item.floor && action.lift === item.lift))
        });
      }, i*3000);

    });
  };

  execution = (actions) => {
    let ascend = actions.filter((action) => action.lift === "up");
    let descend = actions.filter((action) => action.lift === "down");

    ascend.sort(ascending("floor"));

    function ascending(prop) {
      return function (a, b) {
        if (a[prop] > b[prop]) {
          return 1;
        } else if (a[prop] < b[prop]) {
          return -1;
        }
        return 0;
      };
    }

    descend.sort(descending("floor"));

    function descending(prop) {
      return function (a, b) {
        if (a[prop] < b[prop]) {
          return 1;
        } else if (a[prop] > b[prop]) {
          return -1;
        }
        return 0;
      };
    }

    let order = ascend.concat(descend);

    this.changeFloor(order);
  };

  startRun = () => {
    const { actions } = this.state;
    if (actions.length === 0) {
      alert("Please click the lift buttons for desired action.");
    }
    if (actions.length === 1) {
      this.execution(actions);
    }
    if (actions.length > 1) {
      this.execution(actions);
    }
  };

  render() {
    return (
      <React.Fragment>
        <div className="container mt-1">
          <div className="row">
            <div className="col-9">
              <Grid floor={6} state={this.state} addAction={this.addAction} />
              <Grid floor={5} state={this.state} addAction={this.addAction} />
              <Grid floor={4} state={this.state} addAction={this.addAction} />
              <Grid floor={3} state={this.state} addAction={this.addAction} />
              <Grid floor={2} state={this.state} addAction={this.addAction} />
              <Grid floor={1} state={this.state} addAction={this.addAction} />
              <Grid floor={0} state={this.state} addAction={this.addAction} />
              <div className="row border p-2">
                <div className="col text-center">
                  <button
                    className="btn btn-info"
                    onClick={() => this.startRun()}
                  >
                    Start Run
                  </button>
                </div>
              </div>
            </div>
            <div className="col-3">
              <OrderOfActions state={this.state} />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default App;
