import React from "react";

const OrderOfActions = ({ state }) => {
  return (
    <React.Fragment>
      <div className="container">
        <h4>Order Of Action</h4>

        {state.actions.length === 0 ? (
          <span>Waiting for actions.</span>
        ) : (
          state.actions.map((action) => (
            <React.Fragment>
              <button className="btn btn-secondary m-2" disabled>
                {action.floor===0? "G" : action.floor}{" "}
                {action.lift === "up" ? (
                  <i className="fa fa-arrow-up" />
                ) : (
                  <i className="fa fa-arrow-down" />
                )}
              </button>
              <br />
            </React.Fragment>
          ))
        )}
      </div>
    </React.Fragment>
  );
};

export default OrderOfActions;
