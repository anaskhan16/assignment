import React from "react";

const Grid = ({ floor, state, addAction }) => {
  return (
    <div className="row p-3 border">
      <div className="col-4">
        <button
          className={
            state.current_floor === floor
              ? "btn btn-success"
              : "btn btn-outline-secondary"
          }
          style={{pointerEvents : "none"}}
        >
          {floor === 0 ? "Ground" : `Floor ${floor}`}
        </button>
      </div>
      <div className="col-8">
        <div className="row text-center">
          {floor === 0 && (
            <div className="col text-center">
              <button
                className={
                  state.actions.find(
                    (action) => action.floor === floor && action.lift === "down"
                  )
                    ? "btn btn-danger"
                    : "btn btn-outline-danger"
                }
                onClick={() => addAction({ floor: floor, lift: "down" })}
                disabled={state.actions.find(
                  (action) => action.floor === floor && action.lift === "down"
                )}
              >
                Ground Floor
                <i className="fa fa-arrow-down"></i>
              </button>
            </div>
          )}

          {floor > 0  && (
            <div className="col text-center">
              <button className={
                  state.actions.find(
                    (action) => action.floor === floor && action.lift === "up"
                  )
                    ? "btn btn-primary"
                    : "btn btn-outline-primary"
                }
                onClick={() => addAction({ floor: floor, lift: "up" })}
                disabled={state.actions.find(
                  (action) => action.floor === floor && action.lift === "up"
                )}>
                Lift Floor {floor}
                <i className="fa fa-arrow-up"></i>
              </button>
            </div>
          )}

          {floor > 0 && floor < 6 && (
            <div className="col text-center">
              <button className={
                  state.actions.find(
                    (action) => action.floor === floor && action.lift === "down"
                  )
                    ? "btn btn-danger"
                    : "btn btn-outline-danger"
                }
                onClick={() => addAction({ floor: floor, lift: "down" })}
                disabled={state.actions.find(
                  (action) => action.floor === floor && action.lift === "down"
                )}>
                Lift Floor {floor}
                <i className="fa fa-arrow-down"></i>
              </button>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default Grid;
