import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import "bootstrap/dist/js/bootstrap.js";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/scss/bootstrap.scss";
import "../node_modules/font-awesome/css/font-awesome.min.css";


import App from './App.jsx';

import { BrowserRouter } from "react-router-dom";

ReactDOM.render(
  <BrowserRouter><App/></BrowserRouter>,
  document.getElementById("root")
);
